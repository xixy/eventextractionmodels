#coding=utf-8
import os

tagged_data_path = '../../preprocess/data/tagged_data/original/data_with_one_sentence_range/'

train_data_path =  tagged_data_path + 'train.data.one.sentence.txt'
test_data_path = tagged_data_path + 'test.data.one.sentence.txt'
dev_data_path = tagged_data_path + 'dev.data.one.sentence.txt'

# 训练数据存储路径
data_dir = '../data/original'
# data_dir = './data.offset'

char_vocab_path = data_dir + '/char_vocab.txt'
word_vocab_path = data_dir + '/word_vocab.txt'
# tag_vocab_path = data_dir + '/tag_vocab.new.txt'
tag_vocab_path = data_dir + '/tag_vocab.txt'
tag_3_voacb_path = data_dir + '/tag_vocab.3.txt'
tag_17_voacb_path = data_dir + '/tag_vocab.17.txt'

char_word2vec_path = data_dir + '/char_word2vec.dat'
word_word2vec_path = data_dir + '/word_word2vec.dat'

dimension = 100
seg_dimension = 4
seg_max = 5


trimmed_char_word2vec_path = data_dir + '/trimmed_char_word2vec.npz'
trimmed_word_word2vec_path = data_dir + '/trimmed_word_word2vec.npz'

train_data_id_path = data_dir + '/train.id.txt'
dev_data_id_path = data_dir + '/dev.id.txt'
test_data_id_path = data_dir + '/test.id.txt'

train_data_seg_path = data_dir + '/train.id.segmented.txt'
dev_data_seq_path = data_dir + '/dev.id.segmented.txt'
test_data_seg_path = data_dir + '/test.id.segmented.txt'

train_data_id_lm_path = data_dir +  '/train.id.lm.txt'
dev_data_id_lm_path = data_dir + '/dev.id.lm.txt'
test_data_id_lm_path = data_dir + '/test.id.lm.txt'

#seg
seg_mode = 'BMES'
# seg_mode = 'INDEX'

layer_mode = 'layer.6'

full_train_data_id_lm_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/train.id.lm.full.txt'
full_test_data_id_lm_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/test.id.lm.full.txt'
full_dev_data_id_lm_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/dev.id.lm.full.txt'

mtl_train_data_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/train.mtl.txt'
mtl_test_data_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/test.mtl.txt'
mtl_dev_data_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/dev.mtl.txt'
# full_train_data_id_lm_path = data_dir + '/train.new.label.txt'
# full_test_data_id_lm_path = data_dir + '/test.new.label.txt'
# full_dev_data_id_lm_path = data_dir + '/dev.new.label.txt'

# BERT相关的设置
bert_dir = os.path.abspath('.') + '/model/bert_as_feature/original/'
bert_train_input = bert_dir + 'train.input.txt'
bert_train_output = bert_dir + 'train.output.jsonl'

bert_dev_input = bert_dir + 'dev.input.txt'
bert_dev_output = bert_dir + 'dev.output.jsonl'

bert_test_input = bert_dir + 'test.input.txt'
bert_test_output = bert_dir + 'test.output.jsonl'

bert_output = bert_dir + 'output.jsonl'

# shared global variables to be imported from model also
UNK_TOKEN = "#UNK#"
# NUM_TOKEN = "$NUM$"
NONE_TOKEN = "O"



# LM embedding相关
model_dir = os.path.abspath('.') + '/model/bert_as_feature/chinese_L-12_H-768_A-12/'
bert_vocab_file = model_dir + 'vocab.txt'
bert_config_file = model_dir + 'bert_config.json'
bert_init_checkpoint = model_dir + 'bert_model.ckpt'
lm_embedding_dim = 768

bert_layers = [-1]


# 表示合并方法
concat_method = 'concat'
sum_method = 'sum'
gated_method = 'gated'

# joint loss logits计算方法
max_mode = 'max'
sum_mode = 'sum'

# max_length_sentence = 500

