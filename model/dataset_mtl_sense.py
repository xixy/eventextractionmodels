#coding=utf-8
import random

def strQ2B(ustring):
	"""全角转半角"""
	rstring = ""
	for uchar in ustring:
		inside_code=ord(uchar)
		if inside_code == 12288:
		#全角空格直接转换
			inside_code = 32 
		elif (inside_code >= 65281 and inside_code <= 65374):
			#全角字符（除空格）根据关系转化
			inside_code -= 65248

		rstring += chr(inside_code)
	# if rstring != ustring:
	# 	print('corrected')
	return rstring

class dataset_mtl(object):
	"""用来处理文本数据，并且提供数据"""
	def __init__(self, filename, shuffle = True, max_iter = None, idlized = False):
		'''
		Args:
			filename: path to the data file
			max_iter: max number of sentences to yield from this dataset
		'''
		self.filename = filename
		self.max_iter = max_iter
		self.length = None
		self.idlized = idlized
		self.data = []
		self.shuffle = shuffle
		for(x, y, tags_17, tags_3, lm_embeddings, segs, words) in self:
			self.data.append((x, y, tags_17, tags_3, lm_embeddings, segs, words))


	def __iter__(self):
		'''
		支持iteration的遍历操作，每次得到一个句子的词和标注
		Return:
			words[w1, w2, ..., wn]
			tags:[t1, t2, ..., tn]
		'''
		count = 0
		with open(self.filename) as f:
			chars, tags, tags_17, tags_3, lm_embeddings, segs, words, = [], [], [], [], [], [], []
			datas = []
			for line in f:
				# print line
				line = line.strip()
				# 如果是空行，表示句子开始
				if (len(line) == 0 or line.startswith("-DOCSTART-")):
					count += 1
					if self.max_iter is not None and count > self.max_iter:
						break
					yield chars, tags, tags_17, tags_3, lm_embeddings, segs, words
					chars, tags, tags_17, tags_3, lm_embeddings, segs, words, = [], [], [], [], [], [], []
				else:
					elems = line.split(' ')
					char, tag, tag_17, tag_3, = elems[0], elems[1], elems[2], elems[3]

					# char, tag = strQ2B(elems[0]), elems[1]
					# print word, tag

					if self.idlized:
						char = int(char)
						tag = int(tag)
						tag_17 = int(tag_17)
						tag_3 = int(tag_3)

					chars.append(char)
					tags.append(tag)
					tags_3.append(tag_3)
					tags_17.append(tag_17)

					# 如果有语言模型的话的内容的话
					if len(elems) == 7:
						lm_embedding= [float(x) for x in elems[4].split('|')]
						lm_embeddings.append(lm_embedding)

						seg = int(elems[5])
						segs.append(seg)
						word = int(elems[6])
						words.append(word)



	def __len__(self):
		'''
		查看句子数量
		'''
		if self.length is None:
			self.length = 0
			for _ in self:
				self.length += 1
		return self.length

	def get_minibatch(self, batch_size):
		'''
		从数据集中获取minibatch
		Args:
			batch_size: number of sentences in a batch
		'''
		x_batch, y_batch = [],[]
		for(x, y, _, _, lm_embeddings, segs, words) in self:
			if len(x_batch) == batch_size:
				yield x_batch, y_batch
				x_batch, y_batch = [],[]

			x_batch.append(x)
			y_batch.append(y)

		# 处理最后不足一个batch的情况
		if len(x_batch) > 0:
			yield x_batch, y_batch

	def get_minibatch_with_lm_embedding(self, batch_size):
		'''
		从数据集中获取minibatch，同时携带了lm_embedding batch
		Args:
			batch_size: number of sentences in a batch
		'''
		# 做shuffle
		if self.shuffle:
			random.shuffle(self.data)

		x_batch, y_batch, tag_17_batch, tag_3_batch, embedding_batch, segs_batch, words_batch = [],[],[], [], [], [], []
		for(x, y, tags_17, tags_3, lm_embeddings, segs, words) in self.data:
			if len(x_batch) == batch_size:
				yield x_batch, y_batch, tag_17_batch, tag_3_batch, embedding_batch, segs_batch, words_batch
				x_batch, y_batch, tag_17_batch, tag_3_batch, embedding_batch, segs_batch, words_batch = [],[],[],[], [], [], []

			x_batch.append(x)
			y_batch.append(y)
			tag_17_batch.append(tags_17)
			tag_3_batch.append(tags_3)
			embedding_batch.append(lm_embeddings)
			segs_batch.append(segs)
			words_batch.append(words)

		# 处理最后不足一个batch的情况
		if len(x_batch) > 0:
			yield x_batch, y_batch, tag_17_batch, tag_3_batch, embedding_batch, segs_batch, words_batch


if __name__ == '__main__':
	devset = dataset('../../data/original/BMES/layer.6/dev.mtl.txt')
	# print len(devset)
	# for words, tags in devset:
	# 	print words, tags


	# for x_batch, y_batch in devset.get_minibatch(4):
	# 	# 每个句子
	# 	for x in x_batch:
	# 		print x
	# 	# print(x_batch, y_batch)
	# 	break

	for i,(x_batch, y_batch, tag_17_batch, tag_3_batch, embedding_batch, segs_batch, words_batch) in enumerate(devset.get_minibatch_with_lm_embedding(100)):
		for tags, tags_17, tags_3, lm_embedding in zip(y_batch, tag_17_batch, tag_3_batch, embedding_batch):
			print(tags)
			print(tags_17)
			print(tags_3)
			print(len(lm_embedding[0]))
			print(segs_batch)
			print(words_batch)
			break
		break
		# print(i)
		# print(len(embedding_batch)) # 100
		# # print(embedding_batch[0])
		# print(len(embedding_batch[0][0])) # 4608
		# print(len(segs_batch)) # 100
		# print(len(segs_batch[0][0]))
		# for x, segs in zip(x_batch, segs_batch):
		# 	if len(x) != len(segs):
		# 		raise Exception('长度不一致')
		# print(seqs_batch[0])
		
		# # 每个句子
		# for x, words in zip(x_batch, words_batch):
		# 	print(len(x))
		# 	print(len(words))
		# 	# 每个字
		# 	for char_id, word_id in zip(x, words):
		# 		print(char_id, word_id)
		# 		# pass
		# 	break
		# break


