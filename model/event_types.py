#coding=utf-8

event_types = {
	'Life':['Be-Born', 'Marry', 'Divorce', 'Injure', 'Die'],
	'Movement':['Transport'],
	'Transaction':['Transfer-Money', 'Transfer-Ownership'],
	'Business':['Start-Org', 'Merge-Org', 'Declare-Bankruptcy', 'End-Org'],
	'Conflict':['Attack', 'Demonstrate'],
	'Contact':['Meet', 'Phone-Write'],
	'Personnel':['Start-Position', 'End-Position', 'Nominate', 'Elect'],
	'Justice':['Arrest-Jail', 'Release-Parole', 'Trial-Hearing', 'Charge-Indict', 'Sue', 'Convict', 'Sentence', 'Fine', 'Execute', 'Extradite', 'Acquit', 'Appeal', 'Pardon']
}