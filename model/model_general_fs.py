#coding=utf-8
from base_model import *
import numpy as np
from hyperparameter import *
import sys
sys.path.append(os.path.abspath('.'))
from configure import *
from mapping_3 import *
from mapping_17 import *

class seq_tag_model(base_model):
	"""
	fully shared model,支持fine-tune
	"""
	def __init__(self, hp, logger, vocabs, args, char_embeddings = None,word_embeddings = None, fine_tune = False):
		'''
		初始化
		Args:
			hp: 超参数
			logger: 日志
			vocabs: [1,2,3]字、标签、词的vocab
			args: 状态
			char_embeddings: 字向量
			word_embeddings: 词向量
		'''
		super(seq_tag_model, self).__init__(hp, logger, fine_tune)
		self.char_embeddings_vocab = char_embeddings
		self.word_embeddings_vocab = word_embeddings
		self.vocabs = vocabs
		self.vocab_chars = vocabs[0]
		self.vocab_tags = vocabs[1]

		self.vocab_tags_17 = vocabs[2]
		self.vocab_tags_3 = vocabs[3]

		self.vocab_words = vocabs[4]
		self.args = args
		# 做tag映射
		self.tag_mapper_3 = get_map_np(self.vocab_tags)
		self.tag_mapper_17 = get_mapper(self.vocab_tags)


	def build(self):
		'''
		构建可计算图
		'''
		# 添加placeholders
		self.add_placeholders()
		# 添加向量化操作，得到每个词的向量表示
		self.add_word_embeddings_op()
		# 计算logits
		self.add_logits_op()
		# 计算概率
		self.add_pred_op()
		# 计算损失
		self.add_loss_op()

		# 定义训练操作
		self.add_train_op(self.hp.optimizer, self.lr, self.loss,
			self.hp.clip)
		# 创建session和logger
		self.initialize_session() 

	def get_variables_for_training(self):
		'''

		返回需要优化的参数
		可能是fine-tune的参数，去掉shared lstm之外的task-specific参数
		也可能是所有参数
		'''
		all_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
		# 如果不是fine-tune，那么就返回所有参数
		if not self.fine_tune:
			return all_vars

		# 如果是fine-tune，就返回不包含share的参数
		variables = []
		shared_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='bi-lstm-shared')
		
		for var in all_vars:
			if var not in shared_vars:
				variables.append(var)
		return variables


	def add_placeholders(self):
		'''
		定义placeholder，所有的变量
		'''
		# 表示batch中每个句子的字的id表示
		# shape = (batch size, max length of sentence in batch)
		self.char_ids = tf.placeholder(tf.int32, shape = [None, None], name = "char_ids")

		# 表示batch中每个句子的长度
		# shape = (batch size,)
		self.sequence_lengths = tf.placeholder(tf.int32, shape = [None], name = "sequence_lengths")

		# 表示batch中每个句子的每个char的label
		# shape = (batch size, max length of sentence in batch)
		self.labels = tf.placeholder(tf.int32, shape = [None, None], name = "labels")

		self.labels_3 = tf.placeholder(tf.int32, shape = [None, None], name = "labels_3")

		self.labels_17 = tf.placeholder(tf.int32, shape = [None, None], name = "labels_17")

		# dropout
		self.dropout = tf.placeholder(tf.float32, shape = [], name = "dropout")
		# 学习率
		self.lr = tf.placeholder(tf.float32, shape = [], name = "lr")

		# 
		# shape = (batch size, max length of sentence in batch)
		self.seg_ids = tf.placeholder(tf.int32, shape = [None, None], name = 'seg_ids')

		self.word_ids = tf.placeholder(tf.int32, shape = [None, None], name = "word_ids")

		# LM embedding 语言模型的embedding
		# shape = (batch size, max length of sentence in batch, )
		self.lm_embeddings = tf.placeholder(tf.float32, shape = [None, None, lm_embedding_dim * len(bert_layers)], name = 'lm_embedding')

	def add_word_embeddings_op(self):
		'''
		添加embedding操作，包括词向量和字向量
		如果self.embeddings不是None，那么词向量就采用pre-trained vectors，否则自行训练
		字向量是自行训练的
		'''

		# 1.0 add char embedding
		if self.args.add_char:
			self.logger.info("加入字向量")
			with tf.variable_scope("chars"):
				# 如果词向量是None
				if self.char_embeddings_vocab is None:
					self.logger.info("WARNING: randomly initializing word vectors")
					_char_embeddings = tf.get_variable(
						name = '_char_embeddings',
						dtype = tf.float32,
						shape = [len(self.vocab_chars), self.hp.dimension]
						)
				else:
					# 加载已有的词向量
					_char_embeddings = tf.Variable(
						self.char_embeddings_vocab,
						name = '_char_embeddings',
						dtype = tf.float32,
						trainable = not self.hp.use_pretrained_embeddings
						)
				# lookup来获取word_ids对应的embeddings
				# shape = (batch size, max_length_sentence, dim)
				char_embeddings = tf.nn.embedding_lookup(
					_char_embeddings,
					self.char_ids,
					name = 'char_embeddings'
					)
		# 2.0 add lm embedding
		if self.args.add_lm:
			self.logger.info("加入语言模型向量")
			# if char embedding added
			if self.args.add_char:
				char_embeddings = tf.concat([char_embeddings, self.lm_embeddings], axis = -1)
			else:
			# if char embedding not exist
				char_embeddings = self.lm_embeddings


		# 3.0 add seg embedding
		if self.args.add_seg:
			self.logger.info("加入分词信息向量")
			with tf.variable_scope("segs"):
				_seg_embeddings = tf.get_variable(
					name = '_seg_embeddings',
					dtype = tf.float32,
					shape = [seg_max, seg_dimension],
					initializer=tf.random_normal_initializer(mean=0, stddev=1),
					trainable = True
					)
				seg_embeddings = tf.nn.embedding_lookup(
					_seg_embeddings,
					self.seg_ids,
					name = 'seg_embeddings'
					)

			char_embeddings = tf.concat([char_embeddings, seg_embeddings], axis = -1)

		
		# 4.0 add word embedding
		if self.args.add_word:
			self.logger.info("加入词向量")
			with tf.variable_scope("words"):
				# 如果词向量是None
				if self.word_embeddings_vocab is None:
					self.logger.info("WARNING: randomly initializing word vectors")
					_word_embeddings = tf.get_variable(
						name = '_word_embeddings',
						dtype = tf.float32,
						shape = [len(self.vocab_words), self.hp.dimension]
						)
				else:
					# 加载已有的词向量
					_word_embeddings = tf.Variable(
						self.word_embeddings_vocab,
						name = '_word_embeddings',
						dtype = tf.float32,
						trainable = not self.hp.use_pretrained_embeddings
						)
				# lookup来获取word_ids对应的embeddings
				# shape = (batch size, max_length_sentence, dim)
				word_embeddings = tf.nn.embedding_lookup(
					_word_embeddings,
					self.word_ids,
					name = 'word_embeddings'
					)
			
			char_embeddings = tf.concat([char_embeddings, word_embeddings], axis = -1)



		# 进行dropout

		self.word_embeddings = tf.nn.dropout(char_embeddings, self.dropout)


	def add_logits_op(self):
		'''
		定义self.logits，句子中的每个词都对应一个得分向量，维度是tags的维度
		'''
		# 首先对句子进行LSTM
		with tf.variable_scope('bi-lstm-shared'):
			cell_fw = tf.contrib.rnn.LSTMCell(
				self.hp.hidden_size_lstm
				)
			cell_bw = tf.contrib.rnn.LSTMCell(
				self.hp.hidden_size_lstm
				)
			output = tf.nn.bidirectional_dynamic_rnn(
				cell_fw,
				cell_bw,
				self.word_embeddings,
				sequence_length = self.sequence_lengths,
				dtype = tf.float32
				)
			# 取出output
			# shape = (batch size, max_length_sentence, hidden_size_lstm)
			(output_fw, output_bw), _ = output
			# shape = (batch size, max_length_sentence, 2 * hidden_size_lstm)
			output = tf.concat([output_fw, output_bw], axis = -1)
			# 进行dropout
			# shape = (batch size, max_length_sentence, 2 * hidden_size_lstm)
			output = tf.nn.dropout(output, self.dropout)

		# 然后用全联接网络计算概率


		# 计算67-BIO的概率
		with tf.variable_scope('proj_67'):
			W = tf.get_variable(
				name = 'w', 
				dtype = tf.float32,
				shape = [2 * self.hp.hidden_size_lstm, len(self.vocab_tags)]
				)
			b = tf.get_variable(
				name = 'b',
				dtype = tf.float32,
				shape = [len(self.vocab_tags)],
				initializer = tf.zeros_initializer()
				)
			# 取出max_length_sentence
			nsteps = tf.shape(output)[1]

			# shape = (batch size * max_length_sentence, 2 * hidden_size_lstm)
			output = tf.reshape(output, [-1, 2*self.hp.hidden_size_lstm])

			# shape = (batch size * max_length_sentence, vocab_tags_nums)
			pred = tf.matmul(output, W) + b

			# shape = (batch size, max_length_sentence, vocab_tags_nums)
			self.logits = tf.reshape(pred, [-1, nsteps, len(self.vocab_tags)])

		# 计算17-BIO的概率
		if self.hp.joint_lambda_2 > 0.0:
			with tf.variable_scope('proj_17'):
				W_17 = tf.get_variable(
					name = 'w_17', 
					dtype = tf.float32,
					shape = [2 * self.hp.hidden_size_lstm, len(self.vocab_tags_17)]
					)
				b_17 = tf.get_variable(
					name = 'b_17',
					dtype = tf.float32,
					shape = [len(self.vocab_tags_17)],
					initializer = tf.zeros_initializer()
					)

				# shape = (batch size * max_length_sentence, vocab_tags_nums)
				pred_17 = tf.matmul(output, W_17) + b_17

				# shape = (batch size, max_length_sentence, vocab_tags_nums)
				self.logits_17 = tf.reshape(pred_17, [-1, nsteps, len(self.vocab_tags_17)])

		# 计算3-BIO的概率
		if self.hp.joint_lambda_1 > 0.0:
			with tf.variable_scope('proj_3'):
				W_3 = tf.get_variable(
					name = 'w_3', 
					dtype = tf.float32,
					shape = [2 * self.hp.hidden_size_lstm, len(self.vocab_tags_3)]
					)
				b_3 = tf.get_variable(
					name = 'b_3',
					dtype = tf.float32,
					shape = [len(self.vocab_tags_3)],
					initializer = tf.zeros_initializer()
					)

				# shape = (batch size * max_length_sentence, vocab_tags_nums)
				pred_3 = tf.matmul(output, W_3) + b_3

				# shape = (batch size, max_length_sentence, vocab_tags_nums)
				self.logits_3 = tf.reshape(pred_3, [-1, nsteps, len(self.vocab_tags_3)])


	def add_pred_op(self):
		'''
		计算prediction，如果使用crf的话，需要
		'''
		# 取出概率最大的维度的idx
		# shape = (batch size, max_length_sentence)
		if not self.hp.use_crf:
			self.labels_pred = tf.cast(tf.argmax(self.logits, axis=-1), tf.int32)

	def add_loss_op(self):
		'''
		计算损失
		'''
		# 如果使用crf部分
		if self.hp.use_crf:
			# 1. 67-BIO
			with tf.variable_scope('loss_67'):
				log_likelihood, trans_params = tf.contrib.crf.crf_log_likelihood(
					self.logits,
					self.labels,
					self.sequence_lengths
					)
				self.trans_params = trans_params
				self.loss_67 = tf.reduce_mean(-log_likelihood)

			# 2. 17-BIO
			if self.hp.joint_lambda_2 > 0.0:
				with tf.variable_scope('loss_17'):
					log_likelihood_17, trans_params_17 = tf.contrib.crf.crf_log_likelihood(
						self.logits_17,
						self.labels_17,
						self.sequence_lengths
						)
					self.trans_params_17 = trans_params_17
					self.loss_17 = tf.reduce_mean(-log_likelihood_17)

			# 2. 3-BIO
			if self.hp.joint_lambda_1 > 0.0:
				with tf.variable_scope('loss_3'):
					log_likelihood_3, trans_params_3 = tf.contrib.crf.crf_log_likelihood(
						self.logits_3,
						self.labels_3,
						self.sequence_lengths
						)
					self.trans_params_3 = trans_params_3
					self.loss_3 = tf.reduce_mean(-log_likelihood_3)
		# 如果不计算crf部分
		else:
			# 1. 67-BIO
			# shape = (batch size, max_length_sentence)
			losses = tf.nn.sparse_softmax_cross_entrophy_with_logits(
				logits = self.logits,
				labels = self.labels
				)
			mask = tf.sequence_mask(self.sequence_lengths)
			losses = tf.boolean_mask(losses, mask)
			self.loss_67 = tf.reduce_mean(losses)

			# 2. 17-BIO
			if self.hp.joint_lambda_2 > 0.0:
				# shape = (batch size, max_length_sentence)
				losses = tf.nn.sparse_softmax_cross_entrophy_with_logits(
					logits = self.logits_17,
					labels = self.labels_17
					)
				mask = tf.sequence_mask(self.sequence_lengths)
				losses = tf.boolean_mask(losses, mask)
				self.loss_17 = tf.reduce_mean(losses)

			# 3. 3-BIO
			if self.hp.joint_lambda_1 > 0.0:
				# shape = (batch size, max_length_sentence)
				losses = tf.nn.sparse_softmax_cross_entrophy_with_logits(
					logits = self.logits_3,
					labels = self.labels_3
					)
				mask = tf.sequence_mask(self.sequence_lengths)
				losses = tf.boolean_mask(losses, mask)
				self.loss_3 = tf.reduce_mean(losses)
		# 合并loss
		self.logger.info('3-BIO模型参数为' + str(self.hp.joint_lambda_1))
		self.logger.info('17-BIO模型参数为' + str(self.hp.joint_lambda_2))

		self.loss = self.loss_67
		if self.hp.joint_lambda_1 > 0.0:
			self.loss += self.hp.joint_lambda_1 * self.loss_3
		if self.hp.joint_lambda_2 > 0.0:
			self.loss += self.hp.joint_lambda_2 * self.loss_17

		# for tensorboard
		tf.summary.scalar('loss', self.loss)
		tf.summary.scalar('loss_67', self.loss_67)


	def run_epoch(self, train, dev, epoch):
		'''
		运行一个epoch，包括在训练集上训练、dev集上测试，一个epoch中对train集合的所有数据进行训练
		'''
		batch_size = self.hp.batch_size
		nbatches = (len(train) + batch_size - 1) // batch_size

		# chars_batch, segs_batch, words_batch, lm_embeddings_batch
		for i, (chars_batch, labels_batch, labels_17_batch, labels_3_batch, lm_embeddings_batch, segs_batch, words_batch) in enumerate(train.get_minibatch_with_lm_embedding(batch_size)):

			# 检查wo
			# 构造feed_dict,主要是包括：
			# 1. word_ids, word_length
			# 2. char_ids, char_length
			# 3. learning rate
			# 4. dropout keep prob
			fd, _ = self.get_feed_dict(chars_batch, segs_batch, words_batch, lm_embeddings_batch, labels_batch, 
				labels_3_batch, labels_17_batch, self.hp.learning_rate, self.hp.dropout)

			# 执行计算
			_, train_loss, summary = self.sess.run(
				[self.train_op, self.loss, self.merged],
				feed_dict = fd
				)

			# tensorboard
			if i % 10 == 0:
				self.file_writer.add_summary(summary, epoch * nbatches + i)

		# 在dev集上面进行测试
		metrics = self.run_evaluate(dev)

		msg = " - ".join(["{} {:04.2f}".format(k, v)
			for k, v in metrics.items()])
		self.logger.info(msg)

		# 返回f1
		return metrics["f1_2"]

	def pad_sequences(self, sequences, pad_token, nlevels = 1):
		'''
		对sequence进行填充
		Args:
			sequences: a generator of list or tuple
			pad_token: the token to pad with
			nlevels: padding的深度，如果是1，则表示对词进行填充，如果是2表示对字进行填充
		Return:
			a list of list where each sublist has same length
		'''
		# print '--------pad-sequences--------'
		if nlevels == 1:
			# 找到sequences中的句子最大长度
			max_length_sentence = max(map(lambda x: len(x), sequences))
			# 然后直接进行padding
			sequences_padded, sequences_length = self._pad_sequences(sequences, 
				pad_token, max_length_sentence)

		if nlevels == 2:
			# 找到sequence中所有句子的所有单词中字母数最大的单词
			max_length_word = max([max(map(lambda x: len(x), seq)) for seq in sequences])
			# print max_length_word
			sequences_padded, sequences_length = [], []
			for seq in sequences:
				# print seq
				# 将每个句子的每个词都进行填充
				sp, sl = self._pad_sequences(seq, pad_token, max_length_word)
				# print sp, sl
				# 每个句子的字母的表示
				sequences_padded += [sp]
				# 每个句子的字母的长度
				sequences_length += [sl]
			# 然后对句子进行填充
			# batch中最大长度的句子
			max_length_sentence = max(map(lambda x : len(x), sequences))
			# 填充的时候用[0,0,0,0,0]用字母向量进行填充
			sequences_padded, _ = self._pad_sequences(sequences_padded, 
				[pad_token] * max_length_word, max_length_sentence)
			# 得到句子的每个单词的字母的长度 (batch, max_length_sentence, letter_length)
			sequences_length, _ = self._pad_sequences(sequences_length, 0, max_length_sentence)



		return sequences_padded, sequences_length



	def _pad_sequences(self, sequences, pad_token, max_length):
		'''
		对sequences进行填充
		Args:
			pad_token: the token to pad with
		'''
		sequences_padded, sequences_lengths = [], []
		for sequence in sequences:
			sequence = list(sequence)
			# 获取句子长度
			sequences_lengths += [min(len(sequence), max_length)]
			# 进行填充
			sequence = sequence[:max_length] + [pad_token] * max(max_length - len(sequence), 0)
			sequences_padded += [sequence]
		return sequences_padded, sequences_lengths

	def pad_lm_embedding(self, lm_embeddings, pad_token):
		'''
		对语言模型进行padding
		'''
		# 找到batch中句子最大长度
		max_length_sentence = max(map(lambda x: len(x), lm_embeddings))	
		# print('最大句子长度为'+ str(max_length_sentence))
		# 然后直接进行padding
		sequences_padded, sequences_length = self._pad_sequences(lm_embeddings, 
			[pad_token] * lm_embedding_dim * len(bert_layers), max_length_sentence)
		return sequences_padded, sequences_length

# chars_batch, segs_batch, words_batch, lm_embeddings_batch, labels_batch, 
#				labels_3_batch, labels_17_batch, self.hp.learning_rate, self.hp.dropout

	def get_feed_dict(self, chars_batch, segs_batch, words_batch, lm_embeddings_batch, labels_batch = None, 
		labels_3_batch = None, labels_17_batch = None, lr = None, dropout = None):
		'''
		Args:
			chars_batch:
			segs_batch:
			words_batch:
			lm_embeddings_batch
			labels_batch
			labels: list of ids
			lr: learning rate
			dropout: keep prob
		'''
			
		char_ids, sequence_lengths = self.pad_sequences(chars_batch, 0)

		feed = {
			self.char_ids: char_ids, # 句子的词的id表示(batch, max_length_sentence)
			self.sequence_lengths: sequence_lengths # 句子的词的长度(batch, )
		}

		
		if labels_batch is not None:
			labels, _ = self.pad_sequences(labels_batch, 0)
			feed[self.labels] = labels

			labels_17, _ = self.pad_sequences(labels_17_batch, 0)
			feed[self.labels_17] = labels_17

			labels_3, _ = self.pad_sequences(labels_3_batch, 0)
			feed[self.labels_3] = labels_3

		seg_ids, _  = self.pad_sequences(segs_batch, 0)
		feed[self.seg_ids] = seg_ids

		word_ids, _ = self.pad_sequences(words_batch, 0)
		feed[self.word_ids] = word_ids


		# assert len(word_ids) == len(char_ids)
		# for chars, segs in zip(char_ids, seg_ids):
		# 	assert len(chars) == len(segs)
		# print('测试通过')

		lm_embeddings, _ = self.pad_lm_embedding(lm_embeddings_batch, 0)

		feed[self.lm_embeddings] = lm_embeddings


		if lr is not None:
			feed[self.lr] = lr
		if dropout is not None:
			feed[self.dropout] = dropout
		
		return feed, sequence_lengths


	def run_evaluate(self, test, output_all_file = None, output_wrong_file = None, output_class_wrong_file = None):
		'''
		在测试集上运行，并且统计结果，包括precision/recall/accuracy/f1
		Args:
			test: 一个dataset instance
		Returns:
			metrics: dict metrics['acc'] = 98.4
		'''
		accs = []
		# 进行identification的记录
		correct_preds_id, total_correct_id, total_preds_id = 0., 0., 0. 
		# 进行classification的记录
		correct_preds_cl, total_correct_cl, total_preds_cl = 0., 0., 0.
		
		idx_to_tag = {idx : tag for tag, idx in self.vocab_tags.items()}
		idx_to_token = {idx: token for token, idx in self.vocab_chars.items()}
		for words, labels, _, _,  lm_embeddings_batch, segs_batch, words_batch in test.get_minibatch_with_lm_embedding(self.hp.batch_size):
			# predict_batch
			# shape = (batch size, max_length_sentence)
			# shape = (batch size,)
			idx = -1 # 表示这个batch中句子的index
			labels_pred, sequence_lengths = self.predict_batch(words, segs_batch, words_batch, lm_embeddings_batch)

			for lab, lab_pred, length in zip(labels, labels_pred, sequence_lengths):
				idx += 1
				# 取每一句话，长度为length
				# 正确label
				lab = lab[:length]
				# 预测得到的label
				lab_pred = lab_pred[:length]


				# 预测正确的个数
				accs += [a == b for (a, b) in zip(lab, lab_pred)]

				# 真实chunk
				lab_chunks = set(self.get_chunks(lab, self.vocab_tags))
				# 预测chunk
				lab_pred_chunks = set(self.get_chunks(lab_pred, self.vocab_tags))

				# 1. 计算identification效果

				# 真实结果
				chunks_without_label = set([(item[1], item[2]) for item in lab_chunks])
				# 预测结果
				chunks_pred_without_label = set([(item[1], item[2]) for item in lab_pred_chunks])
				# 记录正确的chunk数量
				correct_preds_id += len(chunks_without_label & chunks_pred_without_label)
				# 预测出的chunk数量
				total_preds_id += len(chunks_pred_without_label)
				# 正确的chunk数量
				total_correct_id += len(chunks_without_label)

				# 2. 计算classification效果

				# 记录正确的chunk数量
				correct_preds_cl += len(lab_chunks & lab_pred_chunks)
				# 预测出的chunk数量
				total_preds_cl += len(lab_pred_chunks)
				# 正确的chunk数量
				total_correct_cl += len(lab_chunks)

				# 3. 进行输出
				# 3.1 输出所有结果
				line = words[idx]
				if output_all_file != None:
					for char,tag, tag_pred in zip(line,lab, lab_pred):
						token = idx_to_token[char]
						tag = idx_to_tag[tag]
						tag_pred = idx_to_tag[tag_pred]
						output_all_file.write(token + '	' + tag + '	' + tag_pred + '\n')
					output_all_file.write('\n')
				# 3.2 输出错误结果
				if output_wrong_file != None and lab != lab_pred:
					for char,tag, tag_pred in zip(line,lab, lab_pred):
						token = idx_to_token[char]
						tag = idx_to_tag[tag]
						tag_pred = idx_to_tag[tag_pred]
						output_wrong_file.write(token + '	' + tag + '	' + tag_pred + '\n')
					output_wrong_file.write('\n')

				# 3.3 输出识别正确但是分类错误的结果
				if output_class_wrong_file != None and len(chunks_without_label & chunks_pred_without_label) != len(lab_chunks & lab_pred_chunks):
					for char,tag, tag_pred in zip(line,lab, lab_pred):
						token = idx_to_token[char]
						tag = idx_to_tag[tag]
						tag_pred = idx_to_tag[tag_pred]
						output_class_wrong_file.write(token + '	' + tag + '	' + tag_pred + '\n')
					output_class_wrong_file.write('\n')

		# 1. 计算trigger identification的结果
		# 计算precision，预测出的chunk中有多少是正确的
		p1 = correct_preds_id / total_preds_id if correct_preds_id > 0 else 0
		# 计算recall，预测正确的chunk占了所有chunk的数量
		r1 = correct_preds_id / total_correct_id if correct_preds_id > 0 else 0
		# 计算f1
		f1_1 = 2 * p1 * r1 / (p1 + r1) if correct_preds_id > 0 else 0

		# 2. 计算trigger classification的结果
		# 计算precision，预测出的chunk中有多少是正确的
		p2 = correct_preds_cl / total_preds_cl if correct_preds_cl > 0 else 0
		# 计算recall，预测正确的chunk占了所有chunk的数量
		r2 = correct_preds_cl / total_correct_cl if correct_preds_cl > 0 else 0
		# 计算f1
		f1_2 = 2 * p2 * r2 / (p2 + r2) if correct_preds_cl > 0 else 0
		# 计算accuracy，用预测对的词的比例来进行表示
		acc = np.mean(accs)

		# 返回结果
		return {
			'p1': 100 * p1,
			'r1': 100 * r1,
			'f1_1': 100 * f1_1,
			'p2': 100 * p2,
			'r2': 100 * r2,
			'f1_2': 100 * f1_2
		}



	def predict_batch(self, chars_batch, segs_batch, words_batch, lm_embeddings_batch):
		'''
		对一个batch进行预测，并返回预测结果
		Args:
			words: list of sentences
		Returns:
			labels_pred: list of labels for each sentence
		'''
		# chars_batch, segs_batch, words_batch, lm_embeddings_batch, labels_batch
		fd, sequence_lengths = self.get_feed_dict(chars_batch, segs_batch, words_batch, lm_embeddings_batch, dropout = 1.0)
		if self.hp.use_crf:
			viterbi_sequences = []
			logits, trans_params = self.sess.run(
				[self.logits, self.trans_params],
				feed_dict = fd
				)
			for logit, sequence_length in zip(logits, sequence_lengths):
				logit = logit[:sequence_length]
				viterbi_seq, viterbi_score = tf.contrib.crf.viterbi_decode(
					logit, trans_params)
				viterbi_sequences += [viterbi_seq]

			return viterbi_sequences, sequence_lengths

		else:
			# shape = (batch size, max_length_sentence)
			labels_pred = self.sess.run(self.labels_pred, feed_dict = fd)

			return labels_pred, sequence_lengths


	def get_chunks(self, seq, tags):
		'''
		给定一个序列的tags，将其中的entity和位置取出来
		Args:
			seq: [4,4,0,0,1,2...] 一个句子的label
			tags: dict['I-LOC'] = 2
		Returns:
			list of (chunk_type, chunk_start, chunk_end)

		Examples:
			seq = [4, 5, 0, 3]
			tags = {
			'B-PER' : 4,
			'I-PER' : 5,
			'B-LOC' : 3
			'O' : 0
			}

			Returns:
				chunks = [
					('PER', 0, 2),
					('LOC', 3, 4)
				]
		'''
		idx_to_tag = {idx : tag for tag, idx in tags.items()}
		# print idx_to_tag
		chunks = []

		# 表示当前的chunk的起点和类型
		chunk_start, chunk_type = None, None
		# print seq

		for i, tag_idx in enumerate(seq):
			# 如果不是entity的一部分
			if tag_idx == tags['O']:
				# 如果chunk_type不是None，那么就是一个entity的结束
				if chunk_type != None:
					chunk = (chunk_type, chunk_start, i)
					chunks.append(chunk)
					chunk_start, chunk_type = None, None
				# 如果chunk_type是None，那么就不需要处理
				else:
					pass
			# 如果是BI
			else:
				tag = idx_to_tag[tag_idx]
				# 如果是B
				if tag[0] == 'B':
					# 如果前面有entity，那么这个entity就完成了
					if chunk_type != None:
						chunk = (chunk_type, chunk_start, i)
						chunks.append(chunk)
						chunk_start, chunk_type = None, None

					# 记录开始
					chunk_start = i
					chunk_type = tag[2:]

				# 如果是I
				else:
					if chunk_type != None:
						# 如果chunk_type发生了变化，例如(B-PER, I-PER, I-LOC)，那么就需要将(B-PER, I-PER)归类为chunk
						if chunk_type != tag[2:]:
							chunk = (chunk_type, chunk_start, i)
							chunks.append(chunk)
							chunk_start, chunk_type = None, None
		
		# 处理可能存在的最后一个未结尾的chunk
		if chunk_type != None:
			chunk = (chunk_type, chunk_start, i + 1)
			chunks.append(chunk)
		return chunks

if __name__ == '__main__':
	model = seq_tag_model(None, None, [None,None,None], None)
	seq = [4, 4, 5, 0, 3, 5]
	tags = {
			'B-PER' : 4,
			'I-PER' : 5,
			'B-LOC' : 3,
			'O' : 0
			}
	print model.get_chunks(seq, tags)

