#coding=utf-8
import jieba
global max_len
global max_word
max_len=0

def strQ2B(ustring):
    """全角转半角"""
    rstring = ""
    for uchar in ustring:
        inside_code=ord(uchar)
        if inside_code == 12288:                              #全角空格直接转换            
            inside_code = 32 
        elif (inside_code >= 65281 and inside_code <= 65374): #全角字符（除空格）根据关系转化
            inside_code -= 65248

        rstring += chr(inside_code)
    return rstring

def read_voc(path_voc):
    f_v = open(path_voc,'r')
    idx = 0
    voc = {}
    for line in f_v:
        word = line.strip()
        #print(word)
        voc[word] = idx
        idx+=1
    f_v.close()
    return voc

def getBMES(idx):
    BMES = []
    for i in range(len(idx)):
        if i < len(idx)-1:
            if idx[i] == 1 and idx[i+1] == 1:
                BMES.append(3)
            if idx[i] == 1 and idx[i+1] > 1:
                BMES.append(0)
            if idx[i] > 1 and idx[i+1] >1:
                BMES.append(1)
            if idx[i] > 1 and idx[i+1] ==1:
                BMES.append(2)   
        if i == len(idx)-1:
            if idx[i] == 1:
                BMES.append(3)
            else:
                BMES.append(2)
    assert len(BMES)==len(idx),'len(BMES)!=len(idx)'
    return BMES

def parse(sentence,word_len,voc,seg):
    global max_len
    global max_word
    num2=0
    idx = []
    word_id = []
    skip = 0
    w = 0
    up = True
    seg_list = seg
    #seg_list = ' '.join(jieba.cut(strQ2B(sentence))).split()
    for i in range(len(seg_list)):
        # print(seg_list[i])
        if seg_list[i] in voc:
            w_id = voc[seg_list[i]]
        else:
            w_id = voc['#UNK#']
        if len(seg_list[i])>max_len:
            max_len = len(seg_list[i])
            max_word = seg_list[i]
            #print(seg_list[i])

        length = len(seg_list[i])
        if up:
            # print(sentence)
            # print(seg_list)
            # try:
            #     pass
            # except Exception, e:
            #     raise e
            # finally:
            #     pass
            try:
                skip = word_len[w]
            except:
                print(word_len)
                print(sentence)
                print(seg_list)
                # raise e
            # skip = word_len[w]
        j=1
        if length >= skip:
            while length > 0:
                # print('wordlen:',word_len[w])
                # print(j,w)
                length -= word_len[w]
                idx.append(j)
                word_id.append(w_id)
                j+=1
                w+=1
                up = True
        else : 
            skip -= length
            up = False
    BMES = getBMES(idx)
    return idx,word_id,BMES




def write_data(sentence,words,word_len,word_id,tag,idx,f,BMES,seg_mode):
    if seg_mode == 'BMES':
        idx = BMES
    assert len(sentence)+len(word_len)-sum(word_len)==len(tag),str(len(tag))+' '+str(len(sentence)+len(word_len)-sum(word_len))+' '+sentence+str(tag)
    assert len(tag)==len(idx),sentence+str(len(sentence))+' '+str(len(tag))+' '+str(len(idx))+'\n'+str(tag)+str(idx)
    assert len(tag)==len(word_id)
    assert len(tag)==len(word_len)
    assert len(tag)==len(words)
    for i in range(len(idx)):
        f.write(words[i]+' '+tag[i]+' '+str(idx[i])+' '+str(word_id[i])+'\n')
    f.write('\n')

def concat(path_lm,path_final):
    print('concat...')
    f_lm = open(path_lm,'r')
    f_seg = open('test_data_seg.txt','r')
    f_final = open(path_final,'w')
    i = 0
    line1 = f_lm.readline()
    line2 = f_seg.readline()
    while line1 != '' and line2 != '':
        #print(i)
        i+=1
        if len(line1.strip())<2:
            while len(line1.strip())<2 and line1 != '':
                line1 = f_lm.readline()
            line2 = f_seg.readline()

            f_final.write('\n')
        else:
            # print(line2,len(line2))
            f_final.write(line1.strip()+' '+line2.strip().split()[-2]+' '+line2.strip().split()[-1]+'\n')
            line1 = f_lm.readline()
            line2 = f_seg.readline()
    print('Done')
    f_final.close()
    f_seg.close()
    f_lm.close()


def find_seg(seg, sentence):
    '''
    寻找seg和sentence之间的对齐
    '''
    # seg_line = ''.join(seg)
    new_seg = []
    length = len(sentence)
    seg_line = ''
    for word in seg:
        seg_line += word
        if len(seg_line) == length:
            new_seg.append(word)
            break
        elif len(seg_line) > length:
            new_seg.append(word[:len(seg_line) - length])
            break
        new_seg.append(word)

    return new_seg

def add_word_info(path_data='test.id.txt',path_voc='word_vocab.txt',path_lm='test.id.lm.txt',path_final='test.id.lm.final',path_seg = 'test.id.segmented.txt',seg_mode='BMES'):
    f = open(path_data,'r')
    f2 = open('test_data_seg.txt','w')
    f3 = open(path_seg,'r')
    voc = read_voc(path_voc)
    sentence = ''
    words = []
    word_len = []
    tag = []
    num1 = 0
    num2 = 0

    for line in f:

        data = line.strip().split()
        #print(data)
        if data == []:
            if sentence!='':
                # 取出分词结果
                seg = [x for x in f3.readline().strip().split()]
                seg_line = ''.join(seg)
                if seg_line != sentence:
                    if sentence in seg_line:
                        seg = find_seg(seg, sentence)
                        print(''.join(seg))
                        print(sentence)
                        print('******************')

                    else:
                        print('分词结果:'+seg_line)
                        print('Bert分词结果:'+sentence)
                        print('++++++++++++++++++')
                        raise Exception('分词结果和Bert结果无法统一')

                # print(seg)
                # else:
                idx,word_id,BMES = parse(sentence,word_len,voc,seg)
                for(idx_s, token) in zip(BMES, words):
                    print(token+str(idx_s))
                write_data(sentence,words,word_len,word_id,tag,idx,f2,BMES,seg_mode)
                sentence = ''
                word_len = []
                tag = []
                words = []
            continue
        num1+=1
        sentence += data[0]
        words.append(data[0])
        word_len.append(len(data[0]))
        tag.append(data[1])

    f.close()
    f2.close()
    f3.close()
    concat(path_lm,path_final)
    print('最大长度为' + str(max_len))
    print('最大长度词为' + str(max_word))