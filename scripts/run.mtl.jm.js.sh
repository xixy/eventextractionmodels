# $1 logits mapping method
# $2 gpu
# $3 model 1/4/7

model1='model1'
model4='model4'
model7='model7'

# lambda_1_vars=(0.0 0.1 0.2 0.3)
# lambda_2_vars=(0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5)
# lambda_1_vars=(0.4 0.5)
# lambda_2_vars=(0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1)
lambda_1_vars=(1.0 0.5 0.0)
lambda_2_vars=(1.0 0.5 0.0)
lambda_3=0.5
lambda_4=0.5

# 模型1
if [[ $3 == $model1 ]]
then
	echo "模型1"
	for lambda_1 in ${lambda_1_vars[@]}
	do
		for lambda_2 in ${lambda_2_vars[@]}
		do
			echo $lambda_1
			echo $lambda_2
			python train_jm_js.py --add_char 1 --add_seg 0 --add_word 0 --add_lm 0 --dirpath /data/xxy/event_data/mtl/jm_js/results/$3/$1/$lambda_1/$lambda_2/$lambda_3/$lambda_4 --add_nvidia $2 --add_lambda_1 $lambda_1 --add_lambda_2 $lambda_2 --add_lambda_3 $lambda_3 --add_lambda_4 $lambda_4 --mapping_method $1
		done
	done
elif [[ $3 == $model4 ]]
then
	echo "模型4"
	for lambda_1 in ${lambda_1_vars[@]}
	do
		for lambda_2 in ${lambda_2_vars[@]}
		do
			echo $lambda_1
			echo $lambda_2
			python train_jm_js.py --add_char 1 --add_seg 0 --add_word 0 --add_lm 1 --dirpath /data/xxy/event_data/mtl/jm_js/results/$3/$1/$lambda_1/$lambda_2/$lambda_3/$lambda_4 --add_nvidia $2 --add_lambda_1 $lambda_1 --add_lambda_2 $lambda_2 --add_lambda_3 $lambda_3 --add_lambda_4 $lambda_4 --mapping_method $1
		done
	done
elif [[ $3 == $model3 ]]
then
	echo "模型4"
	for lambda_1 in ${lambda_1_vars[@]}
	do
		for lambda_2 in ${lambda_2_vars[@]}
		do
			echo $lambda_1
			echo $lambda_2
			python train_jm_js.py --add_char 1 --add_seg 1 --add_word 1 --add_lm 0 --dirpath /data/xxy/event_data/mtl/jm_js/results/$3/$1/$lambda_1/$lambda_2/$lambda_3/$lambda_4 --add_nvidia $2 --add_lambda_1 $lambda_1 --add_lambda_2 $lambda_2 --add_lambda_3 $lambda_3 --add_lambda_4 $lambda_4 --mapping_method $1
		done
	done
elif [[ $3 == $model7 ]]
then
	echo "模型7"
	for lambda_1 in ${lambda_1_vars[@]}
	do
		for lambda_2 in ${lambda_2_vars[@]}
		do
			echo $lambda_1
			echo $lambda_2
			python train_jm_js.py --add_char 1 --add_seg 1 --add_word 1 --add_lm 1 --dirpath /data/xxy/event_data/mtl/jm_js/results/$3/$1/$lambda_1/$lambda_2/$lambda_3/$lambda_4 --add_nvidia $2 --add_lambda_1 $lambda_1 --add_lambda_2 $lambda_2 --add_lambda_3 $lambda_3 --add_lambda_4 $lambda_4 --mapping_method $1
		done
	done
fi