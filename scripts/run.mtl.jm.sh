# $1 gpu
# $2 model 1/4/7

model1='model1'
model4='model4'
model7='model7'

lambda_1_vars=(1.0 0.75 0.5 0.25 0.0)
lambda_2_vars=(1.0 0.75 0.5 0.25 0.0)

# 模型1
if [[ $2 == $model1 ]]
then
	echo "模型1"
	for lambda_1 in ${lambda_1_vars[@]}
	do
		for lambda_2 in ${lambda_2_vars[@]}
		do
			echo $lambda_1
			echo $lambda_2
			python train_jm.py --add_char 1 --add_seg 0 --add_word 0 --add_lm 0 --dirpath /data/xxy/event_data/mtl/jm/results/$2/$lambda_1/$lambda_2 --add_nvidia $1 --add_lambda_1 $lambda_1 --add_lambda_2 $lambda_2
		done
	done
elif [[ $2 == $model4 ]]
then
	echo "模型4"
	for lambda_1 in ${lambda_1_vars[@]}
	do
		for lambda_2 in ${lambda_2_vars[@]}
		do
			echo $lambda_1
			echo $lambda_2
			python train_jm.py --add_char 1 --add_seg 0 --add_word 0 --add_lm 1 --dirpath /data/xxy/event_data/mtl/jm/results/$2/$lambda_1/$lambda_2 --add_nvidia $1 --add_lambda_1 $lambda_1 --add_lambda_2 $lambda_2
		done
	done
elif [[ $2 == $model7 ]]
then
	echo "模型7"
	for lambda_1 in ${lambda_1_vars[@]}
	do
		for lambda_2 in ${lambda_2_vars[@]}
		do
			echo $lambda_1
			echo $lambda_2
			python train_jm.py --add_char 1 --add_seg 1 --add_word 1 --add_lm 1 --dirpath /data/xxy/event_data/mtl/jm/results/$2/$lambda_1/$lambda_2 --add_nvidia $1 --add_lambda_1 $lambda_1 --add_lambda_2 $lambda_2
		done
	done
fi