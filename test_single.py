# coding=utf-8
import sys
sys.path.append('./model/bert_as_feature/bert')
from configure import *
from model.vocab import *
from model.model_general_joint import *
from model.hyperparameter import *
from model.log_util import *
import argparse
from model.dataset_mtl import *

def main(args):
	'''
	构造模型
	构造数据
	进行训练
	保存模型
	'''
	# 1. 加载vocab和embeddings
	char_vocab = load_vocab(char_vocab_path)
	tag_vocab = load_vocab(tag_vocab_path)
	tag_vocab_17 = load_vocab(tag_17_voacb_path)
	tag_vocab_3 = load_vocab(tag_3_voacb_path)
	word_vocab = load_vocab(word_vocab_path)


	char_embeddings = get_trimmed_word2vec(trimmed_char_word2vec_path)
	word_embeddings = get_trimmed_word2vec(trimmed_word_word2vec_path)

	# 2. 得到id化的训练数据
	test_dataset = dataset_mtl(mtl_test_data_path, idlized = True, shuffle = False)

	# 3. 构造模型
	hp = hyperparameter(args)
	logger = get_logger(hp.log_path)
	vocabs = [char_vocab, tag_vocab, tag_vocab_17, tag_vocab_3, word_vocab]
	
	model = seq_tag_model(hp, logger, vocabs, args, char_embeddings, word_embeddings)

	model.build()

	# 4. 测试
	logger.info('开始测试')
	model.restore_session()

	output_all_file = open(model.hp.evaluate_output_all_file, 'w')
	output_wrong_file = open(model.hp.evaluate_output_wrong_file, 'w')
	output_class_wrong_file = open(model.hp.evaluate_output_class_wrong_file, 'w')

	model.evaluate(test_dataset, output_all_file, output_wrong_file, output_class_wrong_file)
	output_all_file.close()
	output_wrong_file.close()
	output_class_wrong_file.close()

	logger.info('测试结束')


if __name__ == '__main__':
	# python train_jm.py --add_char 1 --add_seg 0 --add_word 0 --add_lm 1 --dirpath '../results/result.4' --add_nvidia -1
	parser = argparse.ArgumentParser()
	parser.add_argument('--add_char', required=True, type = int, help='是否添加字向量')
	parser.add_argument('--add_seg', required=True , type = int, help='是否添加分词向量')
	parser.add_argument('--add_word', required=True, type = int, help='是否添加词向量')
	parser.add_argument('--add_lm', required=True, type = int, help='是否添加语言模型向量')
	parser.add_argument('--dirpath', required=True, type = str, help='输出路径')
	parser.add_argument('--add_nvidia',required=True, type = str, help='选择显卡编号')
	# 解析参数
	args = parser.parse_args()
	print(args.dirpath)
	# 设置显卡，如果是-1，就不使用显卡,使用CPU进行运算
	if args.add_nvidia in ['0', '1', '2', '3']:
		os.environ['CUDA_VISIBLE_DEVICES']=args.add_nvidia


	# args = parser.parse_args()
	main(args)






