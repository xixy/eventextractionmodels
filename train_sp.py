# coding=utf-8
import sys
sys.path.append('./model/bert_as_feature/bert')
from configure import *
from model.vocab import *
from model.model_general_sp import *
from model.hyperparameter import *
from model.log_util import *
import argparse
from model.dataset_mtl import *

def main(args):
	'''
	构造模型
	构造数据
	进行训练
	保存模型
	'''
	# 1. 加载vocab和embeddings
	char_vocab = load_vocab(char_vocab_path)
	tag_vocab = load_vocab(tag_vocab_path)
	tag_vocab_17 = load_vocab(tag_17_voacb_path)
	tag_vocab_3 = load_vocab(tag_3_voacb_path)
	word_vocab = load_vocab(word_vocab_path)


	char_embeddings = get_trimmed_word2vec(trimmed_char_word2vec_path)
	word_embeddings = get_trimmed_word2vec(trimmed_word_word2vec_path)

	# 2. 得到id化的训练数据
	train_dataset = dataset_mtl(mtl_train_data_path, idlized = True)
	test_dataset = dataset_mtl(mtl_test_data_path, idlized = True)
	dev_dataset = dataset_mtl(mtl_dev_data_path, idlized = True)

	# 3. 构造模型
	hp = hyperparameter(args)

	hp.joint_lambda_1 = args.add_lambda_1
	hp.joint_lambda_2 = args.add_lambda_2
	logger = get_logger(hp.log_path)
	vocabs = [char_vocab, tag_vocab, tag_vocab_17, tag_vocab_3, word_vocab]
	
	model = seq_tag_model(hp, logger, vocabs, args, char_embeddings, word_embeddings, fine_tune = False)

	# 4 训练
	logger.info('开始训练')
	model.build()
	model.train(train_dataset, test_dataset)
	model.close_session()
	tf.reset_default_graph()
	logger.info('训练结束')
	best_score = model.best_score

	# 5. fine-tune
	# 5.1 获取最好的参数
	hp.joint_lambda_2 = 0.0
	hp.joint_lambda_1 = 0.0
	hp.least_epochs = 0
	model = seq_tag_model(hp, logger, vocabs, args, char_embeddings, word_embeddings, fine_tune = True)
	model.best_score = best_score
	# 5.2 构建模型，并优化参数
	logger.info('开始fine-tune')
	model.build()
	model.restore_session()
	model.train(train_dataset, test_dataset)
	logger.info('fine-tune结束')

	# 6. 测试
	logger.info('开始测试')
	model.restore_session()

	output_all_file = open(model.hp.evaluate_output_all_file, 'w')
	output_wrong_file = open(model.hp.evaluate_output_wrong_file, 'w')
	output_class_wrong_file = open(model.hp.evaluate_output_class_wrong_file, 'w')

	model.evaluate(test_dataset, output_all_file, output_wrong_file, output_class_wrong_file)
	output_all_file.close()
	output_wrong_file.close()
	output_class_wrong_file.close()

	logger.info('测试结束')


if __name__ == '__main__':
	# python train_sp.py --add_char 1 --add_seg 0 --add_word 0 --add_lm 1 --dirpath '../results/result.4' --add_nvidia -1 --add_lambda_1 0.5 --add_lambda_2 0.5
	parser = argparse.ArgumentParser()
	parser.add_argument('--add_char', required=True, type = int, help='是否添加字向量')
	parser.add_argument('--add_seg', required=True , type = int, help='是否添加分词向量')
	parser.add_argument('--add_word', required=True, type = int, help='是否添加词向量')
	parser.add_argument('--add_lm', required=True, type = int, help='是否添加语言模型向量')
	parser.add_argument('--dirpath', required=True, type = str, help='输出路径')
	parser.add_argument('--add_nvidia',required=True, type = str, help='选择显卡编号')
	parser.add_argument('--add_lambda_1',required=True, type = float, help='选择3-BIO loss权重')
	parser.add_argument('--add_lambda_2',required=True, type = float, help='选择25-BIO loss权重')
	parser.add_argument('--combine_method', required=True, type = str, help='选择不同模块的表示合并方式')
	# 解析参数
	args = parser.parse_args()
	# 设置显卡，如果是-1，就不使用显卡,使用CPU进行运算
	if args.add_nvidia in ['0', '1', '2', '3']:
		os.environ['CUDA_VISIBLE_DEVICES']=args.add_nvidia


	# args = parser.parse_args()
	main(args)






